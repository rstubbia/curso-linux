#!/bin/bash
RED='\033[0;31m'
fileSource='https://gitlab.com/facundoPaez/curso_linux/-/raw/main/trafico.txt' 

status_code=$(curl --write-out %{http_code} --silent --output /dev/null ${fileSource})

if [[ "$status_code" -ne 200 ]] ; then
  printf "${RED} file is not exist, status code $status_code"
  exit
fi

curl $fileSource --silent | awk -F ";" 'NR==515, NR==525 { print "\033[32m ID: " $1 " -- NOMBRE:" $6" --REGION:" $12 ;}' 
 